<?php

if(isset($_GET['id'])){
    $id = $_GET['id'];
}else{
    $id = 0;
}

?>

<form method="GET">
<?php
    $stmt = $dbh->query('SELECT *
        FROM plante
        INNER JOIN follow ON plante.id = follow.planteID');
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)):
?>
    <li class="list-group-item <?=($row['id_f']==$id)?'select':'';?>"><a class="<?=($row['id_f']==$id)?'select':'';?>" href="/?page=follow&id=<?= $row['id_f']; ?>"><?=$row['nom_plante']?></a></li>
    <?php
    if($row['id_f'] == $id):
    ?>
        <div class="card">
            <img src="<?= $row['photo_plante']; ?>" class="card-img-top" alt="<?=$row['nom_plante']?>"/>
            <li class="list-group-item"><strong>Luminosité :</strong></li>
            <li class="list-group-item"><strong>Humidité :</strong></li>
            <li class="list-group-item"><strong>Température :</strong></li>
            <a class="supp btn btn-primary" href="/?unfollow=<?=$row['id_f']?>" onclick="return window.confirm('Ne plus superviser la plante ?')">- Arrêter de superviser -</a>
        </div>
    <?php endif; ?>
<?php endwhile; ?>
</form>