<?php

require_once '_connection.php';

require_once '_addSupp.php';

if(isset($_GET['page'])){
    $page = $_GET['page'];
}else{
    $page = 'follow';
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/plantes.css">
    <title>Document</title>
</head>
<body class="container">
    <ul class="nav nav-tabs justify-content-center">
    <li class="nav-item">
        <a class="nav-link <?= ($page == 'follow')?'active':''; ?>" href="/?page=follow">Mes plantes</a>
    </li>
    <li class="nav-item">
        <a class="nav-link <?= ($page == 'plante')?'active':''; ?>" href="/?page=plante">Les plantes</a>
    </li>
    <li class="nav-item">
        <a class="nav-link <?= ($page == 'gerer')?'active':''; ?>" href="/?page=gerer">Gérer</a>
    </li>
    </ul>

    <?php

    switch($page){
        case 'follow':
        require_once '_follow.php';
        break;
        case 'plante':
            require_once '_plantes.php';
        break;
        case 'gerer':
            require_once '_gerer.php';
        break;
    }

    ?>

</body>
</html>