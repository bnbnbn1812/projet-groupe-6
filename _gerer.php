<form method="POST">
  <div class="form-group">
    <input type="text" class="form-control" name="nomPlante" placeholder="Nom">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="catgPlante" placeholder="Catégorie">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="dscrpPlante" placeholder="Description">
  </div>
  <div class="form-group">
    <input type="number" max="999" class="form-control" name="tempPlante" placeholder="Température max">
  </div>
  <div class="form-group form-check">
    <p><strong>Luminositer :</strong></p>
    <div class="chck">
      <input type="radio" class="form-check-input" name="lumPlante" value="Ombre">
      <label class="form-check-label" for="lumPlante">Ombre</label>
    </div>
    <div class="chck">
      <input type="radio" class="form-check-input" name="lumPlante" value="Mi-Ombre">
      <label class="form-check-label" for="lumPlante">Mi-Ombre</label>
    </div>
    <div class="chck">
      <input type="radio" class="form-check-input" name="lumPlante" value="Soleil">
      <label class="form-check-label" for="lumPlante">Soleil</label>
    </div>
  </div>
  <div class="form-group form-check">
    <p><strong>Humidité :</strong></p>
    <div class="chck">
      <input type="radio" class="form-check-input" name="humPlante" value="Sec">
      <label class="form-check-label" for="humPlante">Sec</label>
    </div>
    <div class="chck">
      <input type="radio" class="form-check-input" name="humPlante" value="Humide">
      <label class="form-check-label" for="humPlante">Humide</label>
    </div>
    <div class="chck">
      <input type="radio" class="form-check-input" name="humPlante" value="Tres humide">
      <label class="form-check-label" for="humPlante">Tres Humide</label>
    </div>
  </div>
  <div class="form-group form-check">
    <label class="form-check-label" for="photo"><strong>Photo : </strong></label>
    <input type="text" name="photo" placeholder="URL de l'image">
  </div>
  <button type="submit" class="btn btn-primary" name="addBDD">Ajouter</button>
</form>