<?php

if(isset($_GET['unfollow'])){
    $stmt = $dbh->prepare('DELETE FROM follow
    WHERE id_f = :id');
    $stmt->bindValue('id', $_GET['unfollow']);
    $stmt->execute();
}

if(isset($_GET['follow'])){
    $stmt = $dbh->prepare('INSERT INTO follow (planteID) VALUE (:planteID)');
    $stmt->bindValue('planteID', $_GET['follow']);
    $stmt->execute();
    header('Location: /');
}

if(isset($_POST['addBDD'])){
    $stmt = $dbh->prepare('INSERT INTO plante (nom_plante, catg_plante, photo_plante, dscrp_plante, lum_opt_plante, hum_opt_plante, temp_opt_plante) 
        VALUE (:n, :c, :p, :d, :l, :h, :t)');
    $stmt->bindValue('n', $_POST['nomPlante']);
    $stmt->bindValue('c', $_POST['catgPlante']);
    $stmt->bindValue('p', $_POST['photo']);
    $stmt->bindValue('d', $_POST['dscprPlante']);
    $stmt->bindValue('l', $_POST['lumPlante']);
    $stmt->bindValue('h', $_POST['humPlante']);
    $stmt->bindValue('t', $_POST['tempPlante']);
    $stmt->execute();
    header('Location: /?page=plante');
}

?>