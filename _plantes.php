<?php

if(isset($_GET['id'])){
    $id = $_GET['id'];
}else{
    $id = 0;
}

?>

<form method="GET">
<ul class="list-group">
<?php
    $stmt = $dbh->query('SELECT * FROM plante');
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)):
?>
    <li class="list-group-item <?=($row['id']==$id)?'active select':'';?>">
        <a href="/?page=plante&id=<?= $row['id']; ?>#<?=$row['id']?>"><?= $row['nom_plante']; ?></a>
    </li>
    <?php
    if($row['id'] == $id):
    ?>
        <div id="<?=$row['id']?>" class="card">
            <img src="<?=$row['photo_plante']?>" class="card-img-top" alt="<?=$row['nom_plante']?>">
            <div class="card-body">
                <h5 class="card-title"><?=$row['nom_plante']?> (<?=$row['catg_plante']?>)</h5>
                <p class="card-text"><?=$row['dscrp_plante']?></p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Luminosité :</strong> <?=$row['lum_opt_plante']?></li>
                <li class="list-group-item"><strong>Humidité :</strong> <?=$row['hum_opt_plante']?></li>
                <li class="list-group-item"><strong>Température max :</strong> <?=$row['temp_opt_plante']?>°C</li>
                <li class="list-group-item"><strong>Période floraison :</strong> <?=$row['periode_flo_plante']?></li>
            </ul>
            <a href="/?follow=<?=$row['id']?>" class="btn btn-primary add" onclick="return window.confirm('Planter ?')">Ajouter</a>
        </div>
    <?php endif; ?>
<?php endwhile ?>
</ul>
</form>